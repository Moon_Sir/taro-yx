import { Button, View } from "@tarojs/components"
import "./index.scss"

export default function Index() {
  function onChooseImg(e:any){
    console.log(e.detail)
  }

  return (
    <>
      <View>index页面</View>
      <Button openType="chooseAvatar" onChooseAvatar={onChooseImg}>获取头像</Button>
      {/* <input placeholder="请输入昵称" type="nickname" name="nickname" maxLength={32}></input> */}
    </>
  )
}
